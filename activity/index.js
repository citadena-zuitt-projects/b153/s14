function displaySum(num1, num2){
	console.log(`The sum is ${num1 + num2}.`);
};

displaySum(5, 6);

function displayDiff(num1, num2){
	console.log(`The difference is ${num1 - num2}.`);
}; 

displayDiff(24, 9);

function displayProduct(num1, num2){
	console.log(`The product is ${num1 * num2}.`);
};

displayProduct(12, 4);

function displayProduct2(num1, num2){
	return `The product is ${num1 * num2}.`;
};

let product = displayProduct2(7, 9);
console.log(product);