console.log('Hello, everyone');
console.log(1+1);
// Mini-Activity

console.log('Camille');
console.log('Pizza');

console.log('I end my statement without semi-colon');

let name = "Camille";
console.log(name);

let num = 5;
let num1 = 10;
console.log(num);
console.log(num1);
console.log(num + num1);

console.log(name, num, num1);

let myVariable = "Initial Value";
console.log(myVariable);

let myVariable2;
console.log(myVariable2);

myVariable2 = "New Value"
console.log(myVariable2);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

bestFinalFantasy = "Final Fantasy VII";
console.log(bestFinalFantasy);

// not good practice
bestStarWars = "Star Wars 8";
console.log(bestStarWars);

let myVariable3;
console.log(myVariable3);
myVariable3 = "My new variable";
console.log(myVariable3);

const myConstant = "Constant Value"
console.log(myConstant);

/*myConstant = "Updated Value";
console.log(myConstant);*/

const pi = 3.1416;
const boilingPoint = 100;

// Mini-Activity

let role = "Manager";
role = "Director"
const tin = "124444-1230"
let name2 = "Juan Dela Cruz";
console.log(name2, role, tin);

let country = 'Philippines';
let province = "Metro Manila";

console.log(province, country);

let address = province + "," + country;
console.log(address);

let state = "California";
let country1 = "U.S.A";
let state1 = "Florida";
let province1 = "Rizal";
let city = "Tokyo";
let city1 = "Copenhagen";
let country2 = "Japan";

// Mini-Activity

let address1 = province1 + "," + country;
let address2 = state + "," + country1;
let address3 = city + "," + country2;

console.log(address1);
console.log(address2);
console.log(address3);

let numString1 = "5";
let numString2 = "6";
let number1 = 10;
let number2 = 6;

console.log(numString1 + numString2);
console.log(number1 + number2);

let number3 = 5.5;
let number4 = .5;

console.log(number1 + number4);
console.log(number3 + number4);

console.log(numString1 + number1);
console.log(number2 + numString2);

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Stephen Curry and MVP? " + isMVP);
console.log("Is he the current admin? " +isAdmin);

let array1 = ["Goku", "Gohan", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 5000];

console.log(array1);
console.log(array2);

let hero = {
	heroName: "One Punch Man",
	realName: "Saitama",
	powerLevel: 5000,
	isActive: true
}

console.log(hero);


let Lany = ["Paul Klein", "Les Priest", "Jake Goss"];
let bandMember = {
	firstName: "Paul",
	lastName: "Klein",
	isDeveloper: false,
	age: 33
}

console.log(Lany);
console.log(bandMember);

let sampleNull = null;
let sampleUndefined;

console.log(sampleNull);
console.log(sampleUndefined);

let foundResult = null;

let person2 = {
	name: "Peter",
	age: 35
}

console.log(person2.isAdmin);

console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");
console.log("Hi");

function greet(){
	console.log("Hello");
}

greet();
greet();
greet();
greet();
greet();

function showSum(){
	console.log(6+6);
};

showSum();
showSum();
showSum();

// Note: Do not create functions with the same name

// parameter

function printName(name){
	console.log(`My name is ${name}`)
}

// argument
printName("Mary");


function displayNum(number){
	console.log(number);
};

displayNum("5000");

function printMsg(msg){
	console.log(msg);
};

printMsg("JavaScript is fun!");


function displayFullName(firstName, middleName, lastName){
	console.log(`${firstName} ${middleName} ${lastName}`);
};

displayFullName("Peter", "Sanchez", "Parker");

function createFullName(firstName, middleName, lastName){
	return `${firstName} ${middleName} ${lastName}`
	console.log("I will no longer run because the function's value/result has been returned");
};

let fullName1 = createFullName("Tom", "Cruise", "Mapother");
let fullName2 = createFullName("William", "Bradley", "Pitt");
console.log(fullName1);
console.log(fullName2);

function displaySum(num1, num2){
	console.log(`The sum is ${num1 + num2}.`);
};

displaySum(5, 6);

function displayDiff(num1, num2){
	console.log(`The difference is ${num1 - num2}.`);
}; 

displayDiff(24, 9);

function displayProduct(num1, num2){
	console.log(`The product is ${num1 * num2}.`);
};

displayProduct(12, 4);

function displayProduct(num1, num2){
	console.log(`The product is ${num1 * num2}.`);
};

function displayProduct2(num1, num2){
	return `The product is ${num1 * num2}.`;
};

let product = displayProduct2(7, 9);
console.log(product);